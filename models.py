from typing import List

from sqlalchemy import Column, ForeignKey, Integer, Table, Text
from sqlalchemy.ext.asyncio import AsyncResult
from sqlalchemy.future import select
from sqlalchemy.orm import relationship, selectinload

from database import Base, async_session, engine, session


class Recipe(Base):
    __tablename__ = "table_recipes"

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    cooking_time = Column(Integer, nullable=False)
    description = Column(Text, nullable=True)
    count_of_views = Column(Integer, default=0)

    ingredients: List["Ingredient"] = relationship(
        "Ingredient",
        secondary="relation_table",
        lazy="joined",
        back_populates="recipes",
    )

    def __inc_count(self):
        self.count_of_views += 1

    @classmethod
    async def get_list(cls):
        result = await session.execute(
            select(cls.name, cls.cooking_time, cls.count_of_views)
            .order_by(cls.count_of_views.desc())
            .order_by(cls.cooking_time.asc())
        )

        return result.mappings().all()

    @classmethod
    async def get_recipe(cls, recipe_id: int):
        query_result = await session.execute(
            select(cls)
            .where(cls.id == recipe_id)
            .options(selectinload(cls.ingredients))
        )

        recipe = query_result.scalars().one_or_none()

        if recipe:
            recipe.__inc_count()
            await session.commit()
        return recipe

    async def add(self, ingredients_ids):
        async with async_session() as async_sess:
            async with async_sess.begin():
                ingredient_objects = []
                for id in ingredients_ids:
                    ingredient = await Ingredient.get_ingredient(id, async_sess)
                    if ingredient is None:
                        return None
                    ingredient_objects.append(ingredient)

                self.ingredients.extend(ingredient_objects)
                async_sess.add(self)

        return self

    def __str__(self):
        return (
            f"Recipe("
            f"id={self.id}, "
            f"name={self.name}, "
            f"cooking_time={self.cooking_time}, "
            f"ingredients={self.ingredients}"
            f")"
        )


class Ingredient(Base):
    __tablename__ = "table_ingredients"

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)

    recipes: List["Recipe"] = relationship(
        "Recipe",
        secondary="relation_table",
        back_populates="ingredients",
    )

    @classmethod
    async def get_ingredient(cls, ingredient_id, db_async_session):
        ingredient: AsyncResult = await db_async_session.execute(
            select(cls).where(cls.id == ingredient_id)
        )

        return ingredient.scalars().one_or_none()

    async def add(self):

        async with session.begin():
            session.add(self)

    def __str__(self):
        return f"Ingredient(id={self.id}, name={self.name})"


association_table = Table(
    "relation_table",
    Base.metadata,
    Column(
        "recipe_id",
        ForeignKey("table_recipes.id", ondelete="CASCADE"),
        primary_key=True,
    ),
    Column(
        "ingredient_id",
        ForeignKey("table_ingredients.id", ondelete="CASCADE"),
        primary_key=True,
    ),
)


async def creat_all():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


async def close_all():
    await session.close()
    await engine.dispose()

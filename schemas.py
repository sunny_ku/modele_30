from typing import List

from pydantic import BaseModel


class Message(BaseModel):
    message: str


class BaseIngredient(BaseModel):
    name: str


class IngredientOut(BaseIngredient):
    id: int

    class Config:
        orm_mode = True


class IngredientIn(BaseIngredient):
    pass


class BaseRecipe(BaseModel):
    name: str
    cooking_time: int


class Recipe(BaseRecipe):
    id: int
    description: str
    ingredients: List[IngredientOut] = []

    class Config:
        orm_mode = True


class RecipeIn(BaseRecipe):
    description: str
    ingredients: List[int]


class RecipeInList(BaseRecipe):
    count_of_views: int

    class Config:
        orm_mode = True

import unittest

from sqlalchemy import create_engine
from starlette.testclient import TestClient

from database import Base
from routes import app

client = TestClient(app)


class TestApi(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        engine = create_engine("sqlite:///./database.db")
        Base.metadata.drop_all(bind=engine)
        Base.metadata.create_all(bind=engine)

    def test_1_ingredient_adding(self):
        ingredients = ["onion", "carrot", "beet", "potatoes"]
        for ingredient in ingredients:
            with self.subTest():
                response = client.post(
                    "/ingredients/",
                    json={"name": ingredient},
                )
                self.assertEqual(response.status_code, 200)
                self.assertEqual(response.json()["name"], ingredient)

    def test_2_recipe_adding(self):
        data = {
            "name": "borsch",
            "cooking_time": 60,
            "description": "some description",
            "ingredients": [1, 2, 3, 4],
        }
        response = client.post("/recipes/", json=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["ingredients"][3]["name"], "potatoes")

    def test_3_can_get_recipe(self):
        response = client.get("/recipes/1")
        self.assertEqual(response.json()["name"], "borsch")

    def test_4_count_of_view_changing(self):
        response = client.get("/recipes/")
        self.assertEqual(response.json()[0]["count_of_views"], 1)

    def test_6_cannot_add_recipe_with_missing_ingredient(self):
        data = {
            "name": "new_recipe",
            "cooking_time": 60,
            "description": "some description",
            "ingredients": [5],
        }
        response = client.post("/recipes/", json=data)
        self.assertEqual(response.status_code, 400)

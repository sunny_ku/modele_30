from typing import Optional

from fastapi import FastAPI, Path
from fastapi.responses import JSONResponse

import models
from schemas import IngredientIn, IngredientOut, Message, Recipe, RecipeIn, RecipeInList

app = FastAPI()


@app.on_event("startup")
async def shutdown():
    await models.creat_all()


@app.on_event("shutdown")
async def shutdown_handler():
    await models.close_all()


@app.get("/recipes/", response_model=list[RecipeInList])
async def get_recipes_list():
    return await models.Recipe.get_list()


@app.get("/recipes/{recipe_id}", response_model=Optional[Recipe])
async def get_recipe(recipe_id: int = Path(..., title="Id of recipe", ge=1)):
    return await models.Recipe.get_recipe(recipe_id)


@app.post("/ingredients/", response_model=IngredientOut)
async def create_ingredient(item: IngredientIn):
    ingredient = models.Ingredient(**item.dict())
    await ingredient.add()
    return ingredient


@app.post(
    "/recipes/", response_model=Optional[Recipe], responses={400: {"model": Message}}
)
async def create_recipe(item: RecipeIn):
    recipe_as_dict = item.dict()
    ingredients = recipe_as_dict.pop("ingredients")
    recipe = models.Recipe(**recipe_as_dict)
    recipe = await recipe.add(ingredients)
    if recipe is None:
        return JSONResponse(
            status_code=400, content={"message": "ingredient not found"}
        )
    return recipe
